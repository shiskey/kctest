//
// Это приложение - тестовое задание
// для KidControl.
//
// Базовый компонент нашего приложения это location manager.
// Есть несколько вариантов его использования, каждый из которых
// имеет свои преимущества и недостатки.
// Можно использовать как один их них, так и их комбинацию.
//
// В рамках тестового задания нам будет достаточно использовать geofencing
// как достаточно точный и один их самых лояльных к расходу батареи.
// Так же помимо бекграунд режима он позволит нам работать из терминейта.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) CLLocationManager *locationManager;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end

