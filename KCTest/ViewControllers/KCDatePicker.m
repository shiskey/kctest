//
// Пишем здесь свой нескучный дэйт-пикер
// который через делегат будет слать вызовы
// дата выбрана и дата нажата
//

#import "KCDatePicker.h"
#import "KCDatePickerCell.h"
#import "DateHelper.h"


@implementation KCDatePicker {
    int datesCount;
    UICollectionView *mCollectionView;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        datesCount = 100;
        self.selectedDate = [NSDate date];
        self.selectedItem = [NSIndexPath indexPathForRow:0 inSection:0];
        [[NSNotificationCenter defaultCenter]addObserver:self
                                                selector:@selector(updateCollectionView)
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    }
    return self;
}

- (void)updateCollectionView {
    if (mCollectionView) {
        [mCollectionView reloadData];
    }
}

#pragma mark CollectionView delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    
    if (!mCollectionView) {
        mCollectionView = collectionView;
        mCollectionView.decelerationRate = UIScrollViewDecelerationRateFast;
        [collectionView registerNib:[UINib nibWithNibName:@"KCDatePickerCell" bundle:nil]
         forCellWithReuseIdentifier:@"DatePickerCell"];
    }
    return datesCount;
}

- (KCDatePickerCell*)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    KCDatePickerCell *cell =
    [collectionView dequeueReusableCellWithReuseIdentifier:@"DatePickerCell"
                                              forIndexPath:indexPath];
    
    cell.label.text = [DateHelper dateStringBeforeDate:indexPath.row];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView
       willDisplayCell:(KCDatePickerCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row >= datesCount-10) {
        datesCount += 100;
        [collectionView reloadData];
    }
}

- (void)collectionView:(UICollectionView *)collectionView
                didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([indexPath isEqual:self.selectedItem]) {
        [self tapItemAtIndexPath:indexPath];
    } else {
        [collectionView scrollToItemAtIndexPath:indexPath
                               atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                       animated:YES];
        [self selectItemAtIndexPath:indexPath];
    }
}


#pragma mark Scroll delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGPoint centerPoint = CGPointMake(mCollectionView.center.x + mCollectionView.contentOffset.x,
                                    mCollectionView.center.y + mCollectionView.contentOffset.y);
    NSIndexPath *indexPath = [mCollectionView indexPathForItemAtPoint:centerPoint];
    
    [mCollectionView scrollToItemAtIndexPath:indexPath
                           atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                   animated:YES];
    [self selectItemAtIndexPath:indexPath];
}


#pragma mark Self delegate

- (void)tapItemAtIndexPath:(NSIndexPath*)indexPath {
    id<KCDatePickerDelegate> strongDelegate = self.delegate;
    
    if ([strongDelegate respondsToSelector:@selector(kcDatePicker:didTapOnSelectedDate:)]) {
        [strongDelegate kcDatePicker:self didTapOnSelectedDate:self.selectedDate];
    }
}

- (void)selectItemAtIndexPath:(NSIndexPath*)indexPath {
    
    id<KCDatePickerDelegate> strongDelegate = self.delegate;
    
    if ([strongDelegate respondsToSelector:@selector(kcDatePicker:didSelectDate:)]) {
        NSDate *dateToSelect = [DateHelper dateBeforeDate:indexPath.row];
        self.selectedItem = indexPath;
        self.selectedDate = dateToSelect;
        [strongDelegate kcDatePicker:self didSelectDate:self.selectedDate];
    }
}

@end