//
// Лайаут для нашего дэйт-пикера
// здесь мы уменшим скорость скролла, так как
// по дефолту она очень быстрая и добавим
// эффект увеличения
//

#import "KCCollectionViewFlowLayout.h"

@implementation KCCollectionViewFlowLayout {
    CGSize mScreenSize;
}

const int DISTANCE_THRESHOLD = 115;
const int INCREAS_FACTOR = 50;

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
    mScreenSize = [UIScreen mainScreen].bounds.size;
    
    self.itemSize = CGSizeMake(mScreenSize.width/2, 100);
    self.minimumInteritemSpacing = 0.0;
    self.minimumLineSpacing = mScreenSize.width/10;
    self.sectionInset = UIEdgeInsetsMake(0.0, mScreenSize.width/4.0, 0.0, mScreenSize.width/4.0);
}

- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset
                                 withScrollingVelocity:(CGPoint)velocity {
    
    CGFloat offsetAdjustment = MAXFLOAT;
    CGFloat horizontalOffset = proposedContentOffset.x + (mScreenSize.width - self.itemSize.width) / 2.0;
    CGRect targetRect = CGRectMake(proposedContentOffset.x, 0, mScreenSize.width, mScreenSize.height);
    
    NSArray *attributes = [super layoutAttributesForElementsInRect:targetRect];
    
    for (UICollectionViewLayoutAttributes *layoutAttributes in attributes) {
        CGFloat itemOffset = layoutAttributes.frame.origin.x;
        if (fabs((itemOffset - horizontalOffset)) < fabs(offsetAdjustment)) {
            offsetAdjustment = floor(itemOffset - horizontalOffset);
        }
    }
    
    CGFloat offsetX = floor(proposedContentOffset.x) + offsetAdjustment;
    
    return CGPointMake(offsetX, proposedContentOffset.y);
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    
    NSArray *attributes = [[NSArray alloc] initWithArray:[super layoutAttributesForElementsInRect:rect] copyItems:YES];
    CGRect visibleRect;
    visibleRect.origin = CGPointMake(self.collectionView.contentOffset.x+
                                     self.collectionView.bounds.size.width/4,
                                     self.collectionView.contentOffset.y);
    visibleRect.size = CGSizeMake(self.itemSize.width, self.itemSize.height);
    
    for (UICollectionViewLayoutAttributes *attrs in attributes) {
        if (CGRectIntersectsRect(visibleRect, attrs.frame)) {
            
            float distance = CGRectGetMidX(visibleRect) - attrs.center.x;
            float normalizedDistance = distance / DISTANCE_THRESHOLD;
            if (ABS(distance) < DISTANCE_THRESHOLD) {
                float zoom = INCREAS_FACTOR * (1 - ABS(normalizedDistance));
                attrs.frame = CGRectMake(attrs.frame.origin.x,
                                         attrs.frame.origin.y-zoom,
                                         self.itemSize.width,
                                         self.itemSize.height+zoom);
            }
        }
    }
    
    return attributes;
}


@end
