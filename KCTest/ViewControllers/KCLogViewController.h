//
// На этом конроллере будем выводить лог
// наших координат перемещений и показателя батареи
// за выбраную дату
//

#import <UIKit/UIKit.h>

@interface KCLogViewController : UIViewController

@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSMutableArray *locations;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@end
