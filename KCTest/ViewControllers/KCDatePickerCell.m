//
// Округлим края нашей ячейки даты
// и добавим немного тени
//

#import "KCDatePickerCell.h"

@implementation KCDatePickerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.containerView.layer.cornerRadius = 4;
    
    self.layer.masksToBounds = NO;
    self.layer.shadowOffset = CGSizeMake(0, 0);
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowRadius = 2.0f;
    self.layer.shadowOpacity = 0.20f;
}

@end
