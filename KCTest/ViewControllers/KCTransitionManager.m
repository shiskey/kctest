//
// Ничего особенного,
// просто транзишн для
// наглядного перехода на наш log view
//

#import "KCTransitionManager.h"

@implementation KCTransitionManager

- (void)setMainController:(KCMapViewController *)mainController{
    _mainController = mainController;
}


- (void)presentLogViewControllerWithSelectedDate:(NSDate*)date {
    if (!self.logViewController) {
        self.logViewController = [[KCLogViewController alloc] init];
        self.logViewController.date = date;
        self.logViewController.transitioningDelegate = self;
        self.logViewController.modalPresentationStyle = UIModalPresentationCustom;
        [self.mainController presentViewController:self.logViewController
                                          animated:YES
                                        completion:nil];
    }
}

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.4;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    if (self.isPresenting) {
        KCLogViewController *toViewController =
        [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
        KCMapViewController *fromViewController =
        [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
        [[transitionContext containerView] addSubview:toViewController.view];
        
        toViewController.view.frame = fromViewController.startTransitionView.frame;
        toViewController.titleViewHeightConstraint.constant =
        fromViewController.startTransitionView.bounds.size.height;
        toViewController.closeButton.alpha = 0.0;
        toViewController.tableView.alpha = 0.0;
        [toViewController.view layoutIfNeeded];
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext]
                              delay:0.0
             usingSpringWithDamping:1.1
              initialSpringVelocity:1.1
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             fromViewController.view.transform = CGAffineTransformMakeScale(0.9, 0.9);
                             fromViewController.view.alpha = 0.4;
                             
                             toViewController.view.frame = fromViewController.endTransitionView.frame;
                             toViewController.titleViewHeightConstraint.constant =
                             64.0;
                             toViewController.closeButton.alpha = 1.0;
                             toViewController.tableView.alpha = 1.0;
                             [toViewController.view layoutIfNeeded];
                         }
                         completion:^(BOOL finished) {
                             [transitionContext completeTransition:![transitionContext
                                                                     transitionWasCancelled]];
                         }
         ];
    } else {
        KCMapViewController *toViewController =
        [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
        KCLogViewController *fromViewController =
        [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext]
                              delay:0.0
             usingSpringWithDamping:1.1
              initialSpringVelocity:1.1
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             toViewController.view.transform = CGAffineTransformIdentity;
                             toViewController.view.alpha = 1.0;
                             
                             fromViewController.view.frame = toViewController.startTransitionView.frame;
                             fromViewController.titleViewHeightConstraint.constant =
                             toViewController.startTransitionView.bounds.size.height;
                             fromViewController.closeButton.alpha = 0.0;
                             fromViewController.tableView.alpha = 0.0;
                             [fromViewController.view layoutIfNeeded];
                         }
                         completion:^(BOOL finished) {
                             [transitionContext completeTransition:![transitionContext
                                                                     transitionWasCancelled]];
                             self.logViewController = nil;
                         }
         ];
    }
}


#pragma mark UIVieControllerTransitioningDelegate

- (KCTransitionManager *)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    self.isPresenting = YES;
    return self;
}

- (KCTransitionManager *)animationControllerForDismissedController:(UIViewController *)dismissed {
    self.isPresenting = NO;
    return self;
}

@end
