//
// Здесь инициализируется наша map view со своими элементами
// собственно карта, кнопка фокуса на геопозиции и дэйт-пикер.
// скролим дейт-пикер на нужную дату, и за выбранный день строим на карте кривую
// наших локаций. При нажатии на выбраную дату смотрим лог.
//

#import "KCMapViewController.h"
#import "KCLogViewController.h"

#import "KCTransitionManager.h"
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import "Location+CoreDataProperties.h"
#import "DateHelper.h"


@interface KCMapViewController () <MKMapViewDelegate, KCDatePickerDelegate>

@property (nonatomic, strong) KCTransitionManager *transitionManager;

@end

@implementation KCMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Subscribe for local notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(drawPolylineForCurrentDate)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(drawPolylineForCurrentDate)
                                                 name:@"DrawPolyline"
                                               object:nil];
    
    // ViewController properties
    self.datePicker.delegate = self;
    self.transitionManager = [KCTransitionManager new];
    self.transitionManager.mainController = self;
    
    // Focus on current position
    [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    [self focusOnCurrentPosition];
}


#pragma mark MapView

- (void)drawPolylineForCurrentDate {
    if ([[NSCalendar currentCalendar] isDateInToday:self.datePicker.selectedDate]) {
        [self drawPolylineForDate:self.datePicker.selectedDate boundToRect:NO];
    }
}

- (void)drawPolylineForDate:(NSDate*)date boundToRect:(BOOL)boundToRect {
    
    [Location locationsByDate:date block:^(NSArray *objects) {
        
        if (objects.count > 0) {
            // getting coordinates
            long objectsCount = objects.count;
            CLLocationCoordinate2D coordinateArray[objectsCount];
            for (int i=0; i<objects.count; i++) {
                Location *loc = objects[i];
                coordinateArray[i] =
                CLLocationCoordinate2DMake(loc.lat.doubleValue,
                                           loc.lng.doubleValue);
            }
            
            // Remove previous overlay if needed
            if (self.mapView.overlays.count > 0) {
                [self.mapView removeOverlays:self.mapView.overlays];
            }
            
            // Draw polyline
            MKPolyline *polyline = [MKPolyline polylineWithCoordinates:coordinateArray count:objectsCount];
            [self.mapView addOverlay:polyline];
            if (boundToRect) {
                [self.mapView setVisibleMapRect:[polyline boundingMapRect] animated:YES];
            }
        } else {
            // Remove previous overlay if needed
            if (self.mapView.overlays.count > 0) {
                [self.mapView removeOverlays:self.mapView.overlays];
            }
            
            [self focusOnCurrentPosition];
        }
        
    }];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    
    if ([overlay isKindOfClass:MKPolyline.class]) {
        MKPolylineRenderer *polylineRenderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        polylineRenderer.fillColor = [UIColor redColor];
        polylineRenderer.strokeColor = [UIColor redColor];
        polylineRenderer.lineWidth = 2;
        return polylineRenderer;
    }
    
    return nil;
}

- (void)focusOnCurrentPosition {
    [self.mapView
     setRegion:MKCoordinateRegionMakeWithDistance(self.mapView.userLocation.coordinate,
                                                  500, 500)
     animated:YES];
}

- (IBAction)focusOnCurrentPosition:(id)sender {
    [self focusOnCurrentPosition];
}


#pragma mark KCDatePicker delegate

- (void)kcDatePicker:(KCDatePicker *)datePicker didSelectDate:(NSDate *)date {
    [self drawPolylineForDate:date boundToRect:YES];
}

- (void)kcDatePicker:(KCDatePicker *)datePicker didTapOnSelectedDate:(NSDate *)date {
    [self.transitionManager presentLogViewControllerWithSelectedDate:date];
}

@end
