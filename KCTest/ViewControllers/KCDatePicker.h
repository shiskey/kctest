//
// Пишем здесь свой нескучный дэйт-пикер
// который через делегат будет слать вызовы
// дата выбрана и дата нажата
//

#import <UIKit/UIKit.h>

@protocol KCDatePickerDelegate;

@interface KCDatePicker : UIView <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, weak) id<KCDatePickerDelegate> delegate;

@property (nonatomic) NSIndexPath *selectedItem;
@property (nonatomic) NSDate* selectedDate;

@end

@protocol KCDatePickerDelegate <NSObject>

- (void)kcDatePicker:(KCDatePicker*)datePicker didSelectDate:(NSDate*)date;
- (void)kcDatePicker:(KCDatePicker*)datePicker didTapOnSelectedDate:(NSDate*)date;

@end