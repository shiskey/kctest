//
// На этом конроллере будем выводить лог
// наших координат перемещений и показателя батареи
// за выбраную дату
//

#import "KCLogViewController.h"

#import "AppDelegate.h"
#import "DateHelper.h"
#import "Location+CoreDataProperties.h"

@interface KCLogViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation KCLogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // View properties
    self.view.layer.cornerRadius = 4;
    self.view.layer.masksToBounds = YES;
    self.dateLabel.text =
    [DateHelper formattedDateStringFromDate:self.date
                                     format:@"d MMMM"];
    
    [self.tableView registerClass:UITableViewCell.class
           forCellReuseIdentifier:@"LogCell"];
    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    // Setup pull to refresh action
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self
                       action:@selector(pullToRefreshAction:)
             forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];

    // Core Data
    [self fetchData:^{}];
}


#pragma mark TableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.locations.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LogCell"];
    
    Location *location = (Location*)self.locations[indexPath.row];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.text = [NSString stringWithFormat:@"date: %@, \nlat: %@, \nlng: %@, \nbat: %@",
                           [DateHelper formattedDateStringFromDate:location.date format:@"HH:mm:ss, d MMMM"],
                           location.lat ? location.lat : @"battery check",
                           location.lng ? location.lng : @"battery check",
                           location.battery];
    
    return cell;
}

- (void)pullToRefreshAction:(UIRefreshControl *)refreshControl {
    [self fetchData:^{
        [refreshControl endRefreshing];
    }];
}


#pragma mark CoreData

- (void)fetchData:(void(^)())block {
    [Location logByDate:self.date block:^(NSArray *results) {
        self.locations = [NSMutableArray arrayWithArray:results];
        [self.tableView reloadData];
        block();
    }];
}


#pragma mark Navigation

- (IBAction)closeLogViewControllerAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
