//
// Здесь инициализируется наша map view со своими элементами
// собственно карта, кнопка фокуса на геопозиции и дэйт-пикер.
// скролим дейт-пикер на нужную дату, и за выбранный день строим на карте кривую
// наших локаций. При нажатии на выбраную дату смотрим лог.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "KCDatePicker.h"

@interface KCMapViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet KCDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UIView *startTransitionView;
@property (weak, nonatomic) IBOutlet UIView *endTransitionView;


@end
