//
// Округлим края нашей ячейки даты
// и добавим немного тени
//

#import <UIKit/UIKit.h>

@interface KCDatePickerCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
