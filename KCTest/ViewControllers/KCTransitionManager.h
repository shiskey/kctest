//
// Ничего особенного,
// просто транзишн для
// наглядного перехода на наш log view
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "KCMapViewController.h"
#import "KCLogViewController.h"


@interface KCTransitionManager : UIPercentDrivenInteractiveTransition <UIViewControllerTransitioningDelegate, UIViewControllerInteractiveTransitioning>

@property BOOL isPresenting;
@property (nonatomic, weak) KCMapViewController *mainController;
@property (nonatomic, strong) KCLogViewController *logViewController;

- (void)presentLogViewControllerWithSelectedDate:(NSDate*)date;

@end
