//
// Лайаут для нашего дэйт-пикера
// здесь мы уменшим скорость скролла, так как
// по дефолту она очень быстрая и добавим
// эффект увеличения
//

#import <UIKit/UIKit.h>

@interface KCCollectionViewFlowLayout : UICollectionViewFlowLayout

@end
