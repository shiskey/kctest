//
// Эти методы будут помогать
// выводить дату в нужном нам формате
//

#import <Foundation/Foundation.h>

@interface DateHelper : NSObject

+ (NSDate*)dateBeforeDate:(long)daysAgo;
+ (NSString*)dateStringBeforeDate:(long)daysAgo;
+ (NSString*)formattedDateStringFromDate:(NSDate*)date format:(NSString*)format;
+ (NSArray*)getStartAndEndDateFromDate:(NSDate*)date;

@end
