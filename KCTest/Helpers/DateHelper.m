//
// Эти методы будут помогать
// выводить дату в нужном нам формате
//

#import "DateHelper.h"

@implementation DateHelper

+ (NSDate*)dateBeforeDate:(long)daysAgo {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [NSDateComponents new];
    components.day = daysAgo * -1;
    
    return [calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
}

+ (NSString*)dateStringBeforeDate:(long)daysAgo {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [NSDateComponents new];
    components.day = daysAgo * -1;
    
    NSDate *date = [calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
    
    return [self formattedDateStringFromDate:date format:@"d MMMM"];
}

+ (NSString*)formattedDateStringFromDate:(NSDate*)date format:(NSString*)format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = format;
    NSString* dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}
+ (NSArray*)getStartAndEndDateFromDate:(NSDate*)date {
    
    // Start day
    NSDate *startOfDay = [[NSCalendar currentCalendar] startOfDayForDate:date];
    
    // End day
    NSDateComponents *comp = [NSDateComponents new];
    comp.day = 1;
    comp.second = -1;
    NSDate *endOfDay = [[NSCalendar currentCalendar] dateByAddingComponents:comp
                                                                     toDate:startOfDay
                                                                    options:NSCalendarMatchLast];
    
    return @[startOfDay, endOfDay];
}

@end
