//
// Тут будет схема для
// core data сущности
// в которой будем хранить логи
// локаций и батареи.
// Пусть будет называться.. Например Location
//

#import "Location.h"

NS_ASSUME_NONNULL_BEGIN

@interface Location (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) NSNumber *lat;
@property (nullable, nonatomic, retain) NSNumber *lng;
@property (nullable, nonatomic, retain) NSNumber *battery;

@end

NS_ASSUME_NONNULL_END
