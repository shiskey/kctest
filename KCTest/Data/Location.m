//
// Тут будет схема для
// core data сущности
// в которой будем хранить логи
// локаций и батареи.
// Пусть будет называться.. Например Location
//

#import "Location.h"
#import "AppDelegate.h"
#import "DateHelper.h"

@implementation Location

// Creating methods
+ (void)newLocationWithDate:(NSDate*)date
                        lat:(double)lat
                        lng:(double)lng
                    battery:(float)battery {
    
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = app.managedObjectContext;
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass(self.class)
                                              inManagedObjectContext:context];
    Location *location = [[Location alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    location.date = date;
    location.lat = [NSNumber numberWithDouble:lat];
    location.lng = [NSNumber numberWithDouble:lng];
    location.battery = [NSNumber numberWithFloat:battery];
}
+ (void)logBatteryWith:(NSDate*)date
               battery:(float)battery {
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = app.managedObjectContext;
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass(self.class)
                                              inManagedObjectContext:context];
    Location *location = [[Location alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    location.date = date;
    location.lat = nil;
    location.lng = nil;
    location.battery = [NSNumber numberWithFloat:battery];
}

// Fetching methods
+ (void)locationsSortedBy:(NSString*)sortKey ascending:(BOOL)ascending block:(void (^)(NSArray *results))block {
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = app.managedObjectContext;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass(self.class)];
    [request setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:sortKey ascending:ascending]]];
    
    NSAsynchronousFetchRequest *asyncFetch =
    [[NSAsynchronousFetchRequest alloc] initWithFetchRequest:request
                                             completionBlock:^(NSAsynchronousFetchResult *result) {
                                                 block(result.finalResult);
                                             }];
    
    [context performBlock:^{
        [context executeRequest:asyncFetch error:nil];
    }];
}
+ (void)locationsByDate:(NSDate*)date block:(void(^)(NSArray *results))block {
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = app.managedObjectContext;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass(self.class)];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(date >= %@) AND (date < %@) AND lat != null AND lng != null",
                         [DateHelper getStartAndEndDateFromDate:date][0],
                         [DateHelper getStartAndEndDateFromDate:date][1]];
    [request setPredicate:pred];
    [request setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO]]];
    
    NSAsynchronousFetchRequest *asyncFetch =
    [[NSAsynchronousFetchRequest alloc] initWithFetchRequest:request
                                             completionBlock:^(NSAsynchronousFetchResult *result) {
                                                 block(result.finalResult);
                                             }];
    
    [context performBlock:^{
        NSError *err;
        [context executeRequest:asyncFetch error:&err];
    }];
}
+ (void)logByDate:(NSDate*)date block:(void(^)(NSArray *results))block {
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = app.managedObjectContext;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass(self.class)];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(date >= %@) AND (date < %@)",
                         [DateHelper getStartAndEndDateFromDate:date][0],
                         [DateHelper getStartAndEndDateFromDate:date][1]];
    [request setPredicate:pred];
    [request setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO]]];
    
    NSAsynchronousFetchRequest *asyncFetch =
    [[NSAsynchronousFetchRequest alloc] initWithFetchRequest:request
                                             completionBlock:^(NSAsynchronousFetchResult *result) {
                                                 block(result.finalResult);
                                             }];
    
    [context performBlock:^{
        NSError *err;
        [context executeRequest:asyncFetch error:&err];
    }];
}

@end
