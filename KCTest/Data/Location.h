//
// Тут будет схема для
// core data сущности
// в которой будем хранить логи
// локаций и батареи.
// Пусть будет называться.. Например Location
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Location : NSManagedObject

// Creating methods
+ (void)newLocationWithDate:(NSDate*)date
                        lat:(double)lat
                        lng:(double)lng
                    battery:(float)battery;
+ (void)logBatteryWith:(NSDate*)date
               battery:(float)battery;

// Fetching methods
+ (void)locationsSortedBy:(NSString*)sortKey ascending:(BOOL)ascending block:(void (^)(NSArray *results))block;
+ (void)locationsByDate:(NSDate*)date block:(void(^)(NSArray *results))block;
+ (void)logByDate:(NSDate*)date block:(void(^)(NSArray *results))block;

@end

NS_ASSUME_NONNULL_END

#import "Location+CoreDataProperties.h"
