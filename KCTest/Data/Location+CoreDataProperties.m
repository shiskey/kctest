//
// Тут будет схема для
// core data сущности
// в которой будем хранить логи
// локаций и батареи.
// Пусть будет называться.. Например Location
//

#import "Location+CoreDataProperties.h"

@implementation Location (CoreDataProperties)

@dynamic date;
@dynamic lat;
@dynamic lng;
@dynamic battery;

@end
